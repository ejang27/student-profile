package com.example.app.activities;


import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.app.R;
import com.example.app.models.StudentProfile;

import org.parceler.Parcels;

public class EditStudentInfo extends AppCompatActivity implements View.OnClickListener{

    private EditText mFirstName;
    private EditText mLastName;
    private EditText mGender;
    private EditText mSchoolId;
    private EditText mDateOfBirth;
    private EditText mGuardian;
    private EditText mClassName;
    private EditText mTelephone;
    private EditText mNationalID;
    private EditText mAverageGrade;
    private EditText mShoesize;
    private Button mSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_student_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mFirstName = (EditText) findViewById(R.id.edit_student_firstname);
        mLastName = (EditText) findViewById(R.id.edit_student_lastname);
        mGender = (EditText) findViewById(R.id.edit_student_gender);
        mSchoolId = (EditText) findViewById(R.id.edit_student_school_id);
        mDateOfBirth = (EditText) findViewById(R.id.edit_student_birthdate);
        mGuardian = (EditText) findViewById(R.id.edit_student_guadian);
        mClassName = (EditText) findViewById(R.id.edit_student_class_name);
        mTelephone = (EditText) findViewById(R.id.edit_student_phone);
        mNationalID = (EditText) findViewById(R.id.edit_student_nationalID);
        mAverageGrade = (EditText) findViewById(R.id.edit_student_grade);
        mShoesize = (EditText) findViewById(R.id.edit_student_shoeSize);

        StudentProfile input = Parcels.unwrap(getIntent().
                getParcelableExtra("CurrentStudentProfile"));
        setProfile(input);

        mSave = (Button) findViewById(R.id.save);
        mSave.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.save) {
            StudentProfile editedprofile = updateProfile();
            Intent returnIntent = new Intent();
            returnIntent.putExtra("UpdatedStudentProfile", Parcels.wrap(editedprofile));
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }

    }

    public StudentProfile updateProfile() {
        String firstname = mFirstName.getText().toString();
        String lastname = mLastName.getText().toString();
        String gender = mGender.getText().toString();
        String schoolid = mSchoolId.getText().toString();
        String birthdate = mDateOfBirth.getText().toString();
        String guardian = mGuardian.getText().toString();
        String classname = mClassName.getText().toString();
        String telephone = mTelephone.getText().toString();
        String nationalid = mNationalID.getText().toString();
        String grade = mAverageGrade.getText().toString();
        String shoesize = mShoesize.getText().toString();
        return new StudentProfile(firstname, lastname, gender, schoolid, birthdate, guardian,
                classname, telephone, nationalid, grade, shoesize);
    }

    public void setProfile(StudentProfile curProfile) {
        mFirstName.setText(curProfile.getFirstName());
        mLastName.setText(curProfile.getLastName());
        mGender.setText(curProfile.getGender());
        mSchoolId.setText(curProfile.getId());
        mDateOfBirth.setText(curProfile.getDOB());
        mGuardian.setText(curProfile.getGuardian());
        mClassName.setText(curProfile.getClassname());
        mTelephone.setText(curProfile.getTelephone());
        mNationalID.setText(curProfile.getNationalID());
        mAverageGrade.setText(curProfile.getAvegrade());
        mShoesize.setText(curProfile.getShoesize());
    }
}

