package com.example.app.models;

public class Student extends StudentProfile {

    public Student(String firstName, String lastName, String id, boolean attendance) {
        super(firstName, lastName, id, attendance);
    }
}
